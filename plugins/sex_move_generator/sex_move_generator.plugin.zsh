#! /bin/bash

places=("Russian" "Albanian" "Arabic" "Samoan" "Portuguese");
places+=("Dutch" "German" "Bosnian" "Serbian" "Croatian" "Bulgarian");
places+=("Burmese" "Mandarin" "Chinese" "Finnish" "Swedish" "Icelandic");
places+=("Persian" "Irish" "Swahili" "Latvian" "Romanian" "Mongolian");
places+=("Italian" "Thai" "Welsh" "Scottish" "Cornish" "Hawaiian");
places+=("Vietnamese" "Malaysian" "Ethiopian");

nouns=("waffle maker" "fruit salad" "turtle neck" "ice cube");
nouns+=("steam shovel" "manhole cover" "handkerchief " "turkey neck");
nouns+=("light bulb" "bar stool" "boomerang " "pineapple " "dip stick");
nouns+=("creme brulee" "gas mask " "garden hose" "pot holder" "bowling bag");
nouns+=("night light" "door knob" "fruit basket" "chocolate muffin" "carmel apple");
nouns+=("banana peel" "extension cord" "pillow case" "lamp shade" "belt buckle");
nouns+=("couch cushion" "greeting card" "dental floss" "baloon animal" "pencil sharpener");
nouns+=("yogurt pretzel" "glazed doughnut" "stroganoff" "kumquat" "pepper mill");

saySexMove() {
 randPlace=${places[$RANDOM % ${#places[@]}]}
 randNoun=${nouns[$RANDOM % ${#nouns[@]}]}
 move="$randPlace $randNoun"
 echo $move
 say $move
}
